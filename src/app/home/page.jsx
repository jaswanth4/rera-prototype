'use client'
import * as React from "react";
import { styled, useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import MuiDrawer from "@mui/material/Drawer";
import MuiAppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import InboxIcon from "@mui/icons-material/MoveToInbox";
import MailIcon from "@mui/icons-material/Mail";
import profile_img from "../profilepic/JR_NTR9.png";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import Collapse from "@mui/material/Collapse";
import StarBorder from "@mui/icons-material/StarBorder";
import "../sidebar/sidebar"
import upload_pic from "../profilepic/img-Photo.jpg";
import Image from "next/image";

import { useRouter } from "next/navigation";
import { useEffect } from "react";

const drawerWidth = 240;

const openedMixin = (theme) => ({
  width: drawerWidth,
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: "hidden",
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: "hidden",
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up("sm")]: {
    width: `calc(${theme.spacing(8)} + 1px)`,
  },
});

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  ...(open && {
    ...openedMixin(theme),
    "& .MuiDrawer-paper": openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme),
  }),
}));

const data = [
  {
    id: 56354,
    firstName: "NAVANEETH",
    middleName: "KUMAR",
    lastName: "NARISETTY",
    designation: "Proprietor",
    panNumber: "xxxxxx574E",
  },
  {
    id: 56355,
    firstName: "NAVANEETH",
    middleName: "KUMAR",
    lastName: "NARISETTY",
    designation: "Authorized Signatory",
    panNumber: "xxxxxx574E",
  },
  // Add more data as needed
];
export default function MiniDrawer() {
  const theme = useTheme();
  const [open, setOpen] = React.useState(true);
    const [image, setImage] = React.useState(null);
  const [checked, setchecked] = React.useState(null);
  const [steate,handelState]=React.useState(false)
  const router = useRouter();
  const [formdata,setFormdata]=React.useState([])
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handelpost=()=>{

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(obj)
  };
  fetch('http://localhost:3004/profile', requestOptions)
      .then(response => alert("Form submited "))
      
}

  const handleDrawerClose = () => {
    setOpen(false);
  };
  const [nested, setNested] = React.useState(false);
  const [nested2, setNested2] = React.useState(false);

  const handleClick = () => {
    setNested(!nested);
  };
  const handleClick2 = () => {
    setNested2(!nested2);
  };

  const handleImageChange = (e) => {
    const file = e.target.files[0];

    if (file) {
      const reader = new FileReader();

      reader.onload = (event) => {
        setImage(event.target.result);
      };

      reader.readAsDataURL(file);
    } else {
      setImage(null);
    }
  };

  const handleEdit = (id) => {
    // Handle edit action
    console.log(`Edit member with id ${id}`);
  };

  const handleDelete = (id) => {
    // Handle delete action
    console.log(`Delete member with id ${id}`);
  };
  var obj={}
  const handleInd=(name,e)=>{
    obj[e.target.name]=e.target.value
    console.log(e)
  
  }
  const handlestatedata=(vale,val)=>{
 
  }
 useEffect(()=>{

  fetch('http://localhost:3004/profile')
  .then(response => response.json())
  .then(data => setFormdata(data));
      

 },[])

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <div>
        
      </div>
      <Box component="main" sx={{ flexGrow: 1, backgroundColor: "#f6f7fb" }}>
        <Toolbar sx={{ backgroundColor: "white" }}>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{
              marginRight: 5,
              ...(open && { display: "none" }),
            }}
          >
            <MenuIcon />
          </IconButton>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerClose}
            edge="start"
            sx={{
              marginRight: 5,
              ...(!open && { display: "none" }),
            }}
          >
            <MenuIcon />
          </IconButton>

          <div>
            <img
              className="logo_img"
              src="https://rerait.telangana.gov.in/Images/rera%20logo.png"
            />
          </div>

          <Typography variant="h6" noWrap component="div"></Typography>
          <h3 className="header">
            Telangana State Real Estate Regulatory Authority
          </h3>
        </Toolbar>

        <Typography paragraph sx={{ p: 2, backgroundColor: "#f6f7fb" }}>
          <div class="grid-container">
            <div class="headline">My Profile</div>
            <br></br>
            <div class="headline">
              <span
                style={{ color: "red", fontSize: "13px", fontWeight: "bold" }}
              >
                *All * Mark field are mandatory.
              </span>
            </div>
            <br></br>
            <div class="headline">General Information</div>
            <div className="all_forms_div_wrapper">
              <div className="details_fill_wrapper">
                <p className="ot_text">
                  Organization Type <span style={{ color: "white" }}>*</span>
                </p>
                <div className="input_div_wrapper">
                  <input
                  onChange={(e)=>{handleInd(e.name,e)}}
                  defaultValue={""}
                    required
                    className=""
                    name="Organization"
                    
                    type="radio"
                    checked={checked ? false : true}
                    onClick={() => {
                      setchecked(!checked);
                    }}
                  />
                  <span style={{ fontWeight: "bold", fontSize: "14px" }}>
                    Individual
                  </span>{" "}
                  <input
                  onChange={(e)=>{handleInd(e.name,e)}}
                  defaultValue={""}
                    required
                    className=""
                    type="radio"
                    checked={checked ? true : false}
                    onClick={() => {
                      setchecked(!checked);
                    }}
                  />
                  <span style={{ fontWeight: "bold", fontSize: "14px" }}>
                    Other Than Individual
                  </span>
                </div>
              </div>
              {/* <div className="details_fill_wrapper">
                <p className="ot_text">
                  Name <span style={{ color: "red" }}>*</span>
                </p>
                <div className="input_div_wrapper">
                  <input
                  onChange={(e)=>{handleInd(e.name,e)}}
                  defaultValue={""}
                  required
                          className="form_input_control"
                    type="text"
                  />
                </div>
              </div> */}
            </div>
          </div>
          {checked ? (
            <>
            <form>
              <div class="grid-container">
                <div class="headline">Organization Type</div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Organization Type{" "}
                      <span style={{ color: "white" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <select
                        id="cars"
                        name="carlist"
                        form="carform"
                        className="form-control"
                      >
                        <option value="5">Company</option>
                        <option value="6">Partnership</option>
                        <option value="7">Trust</option>
                        <option value="8">Societies</option>
                        <option value="9">Public Authority</option>
                        <option value="27">Others</option>
                        <option selected="selected" value="123">
                          Proprietorship
                        </option>
                        <option value="124">LLP</option>
                      </select>
                    </div>
                  </div>
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Name <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      
                      defaultValue={data||""}
                        
                        className="form_input_control"
                        type="text"
                        required
                      />
                    </div>
                  </div>
                </div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      PAN Number <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      GST Number <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                </div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Do you have any Past Experience?{" "}
                      <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""} required className="" type="radio" />
                      <span
                        style={{ fontWeight: "semibold", fontSize: "10px" }}
                      >
                        YES
                      </span>{" "}
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""} required className="" type="radio" />
                      <span
                        style={{ fontWeight: "semibold", fontSize: "10px" }}
                      >
                        NO
                      </span>
                    </div>
                  </div>
                </div>
                <div class="headline">Address Details</div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      House No/Sy. No/Block No/Plot No{" "}
                      <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Building Name <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                </div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Street Name <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Locality <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                </div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Land mark <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      State <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <select
                        id="cars"
                        name="carlist"
                        form="carform"
                        className="form-control"
                      >
                        <option value="">Select State</option>
                        <option value="4">ANDAMAN &amp; NICOBAR ISLANDS</option>
                        <option value="5">ANDHRA PRADESH</option>
                        <option value="6">ARUNACHAL PRADESH</option>
                        <option value="7">ASSAM</option>
                        <option value="8">BIHAR</option>
                        <option value="9">CHANDIGARH</option>
                        <option value="10">CHHATTISGARH</option>
                        <option value="11">DADRA &amp; NAGAR HAVELI</option>
                        <option value="12">DAMAN &amp; DIU</option>
                        <option value="13">GOA</option>
                        <option value="14">GUJARAT</option>
                        <option value="15">HARYANA</option>
                        <option value="16">HIMACHAL PRADESH</option>
                        <option value="17">JAMMU &amp; KASHMIR</option>
                        <option value="18">JHARKHAND</option>
                        <option value="19">KARNATAKA</option>
                        <option value="3">Kerala</option>
                        <option value="20">LAKSHADWEEP</option>
                        <option value="21">MADHYA PRADESH</option>
                        <option value="2">Maharashtra</option>
                        <option value="22">MANIPUR</option>
                        <option value="23">MEGHALAYA</option>
                        <option value="24">MIZORAM</option>
                        <option value="25">NAGALAND</option>
                        <option value="26">NCT OF DELHI</option>
                        <option value="27">ODISHA</option>
                        <option value="28">PUDUCHERRY</option>
                        <option value="29">PUNJAB</option>
                        <option value="30">RAJASTHAN</option>
                        <option value="31">SIKKIM</option>
                        <option value="32">TAMIL NADU</option>
                        <option selected="selected" value="1">
                          Telangana
                        </option>
                        <option value="33">TRIPURA</option>
                        <option value="34">UTTAR PRADESH</option>
                        <option value="35">UTTARAKHAND</option>
                        <option value="36">WEST BENGAL</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      District <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <select
                        id="cars"
                        name="carlist"
                        form="carform"
                        className="form-control"
                      >
                        <option value="">Select District</option>
                        <option value="1">Adilabad</option>
                        <option value="9">Bhadradri Kothagudem </option>
                        <option selected="selected" value="25">
                          Hyderabad
                        </option>
                        <option value="28">Jagithyal</option>
                        <option value="6">Jangaon</option>
                        <option value="5">Jayashankar</option>
                        <option value="17">Jogulamba Gadwal</option>
                        <option value="2">Kamareddy</option>
                        <option value="27">Karimnagar</option>
                        <option value="8">Khammam</option>
                        <option value="26">Kumuram Bheem (Asifabad)</option>
                        <option value="7">Mahabubabad</option>
                        <option value="14">Mahabubnagar</option>
                        <option value="12">Mancherial</option>
                        <option value="10">Medak</option>
                        <option value="22">Medchal-Malkajgiri</option>
                        <option value="16">Nagarkurnool</option>
                        <option value="18">Nalgonda</option>
                        <option value="23">Nirmal</option>
                        <option value="31">Nizamabad</option>
                        <option value="29">Peddapalli</option>
                        <option value="30">Rajanna Sircilla</option>
                        <option value="24">Ranga Reddy</option>
                        <option value="11">Sangareddy</option>
                        <option value="13">Siddipet</option>
                        <option value="19">Suryapet</option>
                        <option value="21">Vikarabad</option>
                        <option value="15">Wanaparthy</option>
                        <option value="4">Warangal Rural </option>
                        <option value="3">Warangal Urban</option>
                        <option value="20">Yadadri Bhuvanagiri</option>
                      </select>
                    </div>
                  </div>
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Mandal <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <select
                        id="cars"
                        name="carlist"
                        form="carform"
                        className="form-control"
                      >
                        <option value="">Select Mandal</option>
                        <option selected="selected" value="458">
                          Amberpet
                        </option>
                        <option value="472">AMEERPET</option>
                        <option value="468">ASIF NAGAR</option>
                        <option value="462">Bahadurpura</option>
                        <option value="471">BALNAGAR</option>
                        <option value="457">BANDLAGUDA</option>
                        <option value="460">Charminar</option>
                        <option value="469">Golconda</option>
                        <option value="464">Himayatha Nagar</option>
                        <option value="467">KHAIRTABAD</option>
                        <option value="465">Maredpally</option>
                        <option value="463">Musheerabad</option>
                        <option value="461">Nampally</option>
                        <option value="459">Saidabad</option>
                        <option value="466">Secunderabad</option>
                        <option value="474">Secunderabad-Cantonment</option>
                        <option value="473">Secunderabad-Rasoolpura</option>
                        <option value="470">Shaikpet</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Village/City/Town <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <select
                        id="cars"
                        name="carlist"
                        form="carform"
                        className="form-control"
                      >
                        <option value="">Select Village</option>
                        <option value="20442">ADIKMET</option>

                        <option value="20443">ADIKMET (Blighted)</option>
                        <option value="20463">AHMED NAGAR</option>
                        <option value="20450">AKASH NAGAR</option>
                        <option value="20257">
                          Ambedkar Nagar(Slum+Blighted)
                        </option>
                        <option value="20432">ANJAIAH NAGAR</option>
                        <option value="20524">
                          ANJAIAH NAGAR (Tilak Nagar)
                        </option>
                        <option value="20499">ANJUMAN BADA</option>
                        <option value="20489">ANNAPURNA NAGAR</option>
                        <option value="20438">AZAMABAD</option>
                        <option value="20379">AZAMPURA BLIGHTED</option>
                        <option value="20453">AZATH NAGAR</option>
                        <option value="20456">BAPU NAGAR</option>
                        <option value="20535">BATHUKAMMAKUNTA</option>
                        <option value="20528">BHAGYA NAGAR</option>
                        <option value="20538">
                          BHARATH NAGAR (Tilak Nagar )
                        </option>
                        <option value="20464">CE COLONY</option>
                        <option value="20383">CHANCHAL GUDA BLIGHTED</option>
                        <option value="20457">CHENNA REDDY NAGAR</option>
                        <option value="20459">CPL QUATERS</option>
                        <option value="20513">CPL SINGLE QUARTERS </option>
                        <option value="20434">DAYANAND NAGAR</option>
                        <option value="20378">DC PATEL COLONY BLIGHTED</option>
                        <option value="20448">DD COLONY-I-blighted</option>
                        <option value="20454">DD COLONY-II</option>
                        <option value="20532">F G NAGAR</option>
                        <option value="20374">FARATH NAGAR </option>
                        <option value="20380">FARTH NAGAR BLIGHTED</option>
                        <option value="20386">GACHI BOWLI</option>
                        <option value="20428">GANDHI NAGAR</option>
                        <option value="20486">GANGA NAGAR (Harajpenta)</option>
                        <option value="20502">HARRAJPENTA</option>
                      </select>
                    </div>
                  </div>
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Pin code <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                </div>
                <div class="headline">Organization Contact Details</div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Name of Contact Person{" "}
                      <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Designation of Contact Person{" "}
                      <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                </div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Mobile Number <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Secondary Mobile Number{" "}
                      <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                </div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Office Number <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Fax Number<span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                </div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Email ID <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Website URL <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                </div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Village/City/Town <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <select
                        id="cars"
                        name="carlist"
                        form="carform"
                        className="form-control"
                      >
                        <option value="">Select Village</option>
                        <option value="20442">ADIKMET</option>

                        <option value="20443">ADIKMET (Blighted)</option>
                        <option value="20463">AHMED NAGAR</option>
                        <option value="20450">AKASH NAGAR</option>
                        <option value="20257">
                          Ambedkar Nagar(Slum+Blighted)
                        </option>
                        <option value="20432">ANJAIAH NAGAR</option>
                        <option value="20524">
                          ANJAIAH NAGAR (Tilak Nagar)
                        </option>
                        <option value="20499">ANJUMAN BADA</option>
                        <option value="20489">ANNAPURNA NAGAR</option>
                        <option value="20438">AZAMABAD</option>
                        <option value="20379">AZAMPURA BLIGHTED</option>
                        <option value="20453">AZATH NAGAR</option>
                        <option value="20456">BAPU NAGAR</option>
                        <option value="20535">BATHUKAMMAKUNTA</option>
                        <option value="20528">BHAGYA NAGAR</option>
                        <option value="20538">
                          BHARATH NAGAR (Tilak Nagar )
                        </option>
                        <option value="20464">CE COLONY</option>
                        <option value="20383">CHANCHAL GUDA BLIGHTED</option>
                        <option value="20457">CHENNA REDDY NAGAR</option>
                        <option value="20459">CPL QUATERS</option>
                        <option value="20513">CPL SINGLE QUARTERS </option>
                        <option value="20434">DAYANAND NAGAR</option>
                        <option value="20378">DC PATEL COLONY BLIGHTED</option>
                        <option value="20448">DD COLONY-I-blighted</option>
                        <option value="20454">DD COLONY-II</option>
                        <option value="20532">F G NAGAR</option>
                        <option value="20374">FARATH NAGAR </option>
                        <option value="20380">FARTH NAGAR BLIGHTED</option>
                        <option value="20386">GACHI BOWLI</option>
                        <option value="20428">GANDHI NAGAR</option>
                        <option value="20486">GANGA NAGAR (Harajpenta)</option>
                        <option value="20502">HARRAJPENTA</option>
                      </select>
                    </div>
                  </div>
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Pin code <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div class="grid-container">
                <div class="headline">Partner Contact Details</div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Designation <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      First Name <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                </div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Middle Name
                      <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Last Name <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                </div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      PAN Number <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Aadhar Number <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                </div>
                <div class="headline">Address Details</div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      House No/Sy. No/Block No/Plot No{" "}
                      <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Building Name <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                </div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Street Name <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Locality <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                </div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Land mark <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      State <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <select
                        id="cars"
                        name="carlist"
                        form="carform"
                        className="form-control"
                      >
                        <option value="">Select State</option>
                        <option value="4">ANDAMAN &amp; NICOBAR ISLANDS</option>
                        <option value="5">ANDHRA PRADESH</option>
                        <option value="6">ARUNACHAL PRADESH</option>
                        <option value="7">ASSAM</option>
                        <option value="8">BIHAR</option>
                        <option value="9">CHANDIGARH</option>
                        <option value="10">CHHATTISGARH</option>
                        <option value="11">DADRA &amp; NAGAR HAVELI</option>
                        <option value="12">DAMAN &amp; DIU</option>
                        <option value="13">GOA</option>
                        <option value="14">GUJARAT</option>
                        <option value="15">HARYANA</option>
                        <option value="16">HIMACHAL PRADESH</option>
                        <option value="17">JAMMU &amp; KASHMIR</option>
                        <option value="18">JHARKHAND</option>
                        <option value="19">KARNATAKA</option>
                        <option value="3">Kerala</option>
                        <option value="20">LAKSHADWEEP</option>
                        <option value="21">MADHYA PRADESH</option>
                        <option value="2">Maharashtra</option>
                        <option value="22">MANIPUR</option>
                        <option value="23">MEGHALAYA</option>
                        <option value="24">MIZORAM</option>
                        <option value="25">NAGALAND</option>
                        <option value="26">NCT OF DELHI</option>
                        <option value="27">ODISHA</option>
                        <option value="28">PUDUCHERRY</option>
                        <option value="29">PUNJAB</option>
                        <option value="30">RAJASTHAN</option>
                        <option value="31">SIKKIM</option>
                        <option value="32">TAMIL NADU</option>
                        <option selected="selected" value="1">
                          Telangana
                        </option>
                        <option value="33">TRIPURA</option>
                        <option value="34">UTTAR PRADESH</option>
                        <option value="35">UTTARAKHAND</option>
                        <option value="36">WEST BENGAL</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      District <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <select
                        id="cars"
                        name="carlist"
                        form="carform"
                        className="form-control"
                      >
                        <option value="">Select District</option>
                        <option value="1">Adilabad</option>
                        <option value="9">Bhadradri Kothagudem </option>
                        <option selected="selected" value="25">
                          Hyderabad
                        </option>
                        <option value="28">Jagithyal</option>
                        <option value="6">Jangaon</option>
                        <option value="5">Jayashankar</option>
                        <option value="17">Jogulamba Gadwal</option>
                        <option value="2">Kamareddy</option>
                        <option value="27">Karimnagar</option>
                        <option value="8">Khammam</option>
                        <option value="26">Kumuram Bheem (Asifabad)</option>
                        <option value="7">Mahabubabad</option>
                        <option value="14">Mahabubnagar</option>
                        <option value="12">Mancherial</option>
                        <option value="10">Medak</option>
                        <option value="22">Medchal-Malkajgiri</option>
                        <option value="16">Nagarkurnool</option>
                        <option value="18">Nalgonda</option>
                        <option value="23">Nirmal</option>
                        <option value="31">Nizamabad</option>
                        <option value="29">Peddapalli</option>
                        <option value="30">Rajanna Sircilla</option>
                        <option value="24">Ranga Reddy</option>
                        <option value="11">Sangareddy</option>
                        <option value="13">Siddipet</option>
                        <option value="19">Suryapet</option>
                        <option value="21">Vikarabad</option>
                        <option value="15">Wanaparthy</option>
                        <option value="4">Warangal Rural </option>
                        <option value="3">Warangal Urban</option>
                        <option value="20">Yadadri Bhuvanagiri</option>
                      </select>
                    </div>
                  </div>
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Mandal <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <select
                        id="cars"
                        name="carlist"
                        form="carform"
                        className="form-control"
                      >
                        <option value="">Select Mandal</option>
                        <option selected="selected" value="458">
                          Amberpet
                        </option>
                        <option value="472">AMEERPET</option>
                        <option value="468">ASIF NAGAR</option>
                        <option value="462">Bahadurpura</option>
                        <option value="471">BALNAGAR</option>
                        <option value="457">BANDLAGUDA</option>
                        <option value="460">Charminar</option>
                        <option value="469">Golconda</option>
                        <option value="464">Himayatha Nagar</option>
                        <option value="467">KHAIRTABAD</option>
                        <option value="465">Maredpally</option>
                        <option value="463">Musheerabad</option>
                        <option value="461">Nampally</option>
                        <option value="459">Saidabad</option>
                        <option value="466">Secunderabad</option>
                        <option value="474">Secunderabad-Cantonment</option>
                        <option value="473">Secunderabad-Rasoolpura</option>
                        <option value="470">Shaikpet</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Village/City/Town <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <select
                        id="cars"
                        name="carlist"
                        form="carform"
                        className="form-control"
                      >
                        <option value="">Select Village</option>
                        <option value="20442">ADIKMET</option>

                        <option value="20443">ADIKMET (Blighted)</option>
                        <option value="20463">AHMED NAGAR</option>
                        <option value="20450">AKASH NAGAR</option>
                        <option value="20257">
                          Ambedkar Nagar(Slum+Blighted)
                        </option>
                        <option value="20432">ANJAIAH NAGAR</option>
                        <option value="20524">
                          ANJAIAH NAGAR (Tilak Nagar)
                        </option>
                        <option value="20499">ANJUMAN BADA</option>
                        <option value="20489">ANNAPURNA NAGAR</option>
                        <option value="20438">AZAMABAD</option>
                        <option value="20379">AZAMPURA BLIGHTED</option>
                        <option value="20453">AZATH NAGAR</option>
                        <option value="20456">BAPU NAGAR</option>
                        <option value="20535">BATHUKAMMAKUNTA</option>
                        <option value="20528">BHAGYA NAGAR</option>
                        <option value="20538">
                          BHARATH NAGAR (Tilak Nagar )
                        </option>
                        <option value="20464">CE COLONY</option>
                        <option value="20383">CHANCHAL GUDA BLIGHTED</option>
                        <option value="20457">CHENNA REDDY NAGAR</option>
                        <option value="20459">CPL QUATERS</option>
                        <option value="20513">CPL SINGLE QUARTERS </option>
                        <option value="20434">DAYANAND NAGAR</option>
                        <option value="20378">DC PATEL COLONY BLIGHTED</option>
                        <option value="20448">DD COLONY-I-blighted</option>
                        <option value="20454">DD COLONY-II</option>
                        <option value="20532">F G NAGAR</option>
                        <option value="20374">FARATH NAGAR </option>
                        <option value="20380">FARTH NAGAR BLIGHTED</option>
                        <option value="20386">GACHI BOWLI</option>
                        <option value="20428">GANDHI NAGAR</option>
                        <option value="20486">GANGA NAGAR (Harajpenta)</option>
                        <option value="20502">HARRAJPENTA</option>
                      </select>
                    </div>
                  </div>
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      Pin code <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        className="form_input_control"
                        type="text"
                      />
                    </div>
                  </div>
                </div>
                <div className="all_forms_div_wrapper">
                  <div className="details_fill_wrapper">
                    <p className="ot_text">
                      <input
                      onChange={(e)=>{handleInd(e.name,e)}}
                      defaultValue={""}
                        required
                        type="file"
                     
                      />
                      {
                        <div
                          style={{
                            marginTop: "20px",
                            maxWidth: "100px",
                            maxHeight: "100px",
                            overflow: "hidden",
                            border: "1px solid #ccc",
                          }}
                        >
                          {image ? (
                            <Image
                              src={image}
                              width={100}
                              height={100}
                              alt="Picture of the author"
                            />
                          ) : (
                            <Image
                              src={upload_pic}
                              width={100}
                              height={100}
                              alt="Picture of the author"
                            />
                          )}
                        </div>
                      }
                      <div></div> <span style={{ color: "red" }}>*</span>
                    </p>
                    <div className="input_div_wrapper"></div>
                  </div>
                </div>
              </div>
              <center>
               
                <input
                   onClick={()=>{handelpost()}}
                  defaultValue={""}
                    
                    type="submit"
                    name="Save profile"
                    style={{
                      width: "120px",
                      height: "40px",
                      backgroundColor: "#5bc0de",
                      borderColor: "1px solid #5bc0de",
                      borderRadius: "2px",
                      color: "white",
                      fontSize: "14px",
                    }}
                  />
              </center>
              </form>
              <table className="tbl_memberData">
                <tbody>
                  <tr className="heading_row">
                    <th>First Name</th>
                    <th>Middle Name</th>
                    <th>Last Name</th>
                    <th>Designation</th>
                    <th>PAN Number</th>
                    <th>Action</th>
                  </tr>
                  {data.map((member) => (
                    <tr key={member.id} className="row_value_text">
                      <td>{member.firstName}</td>
                      <td>{member.middleName}</td>
                      <td>{member.lastName}</td>
                      <td className="DesiValue">{member.designation}</td>
                      <td>{member.panNumber}</td>
                      <td>
                        <img
                          src="../Images/Edit.jpg"
                          onClick={() => handleEdit(member.id)}
                          width="20px"
                          height="20px"
                          alt="Edit"
                        />
                        {" | "}
                        <img
                          src="../Images/delete.png"
                          onClick={() => handleDelete(member.id)}
                          width="20px"
                          height="20px"
                          alt="Delete"
                        />
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
              
            </>
          ) : (
            <>
              <form>
                <div class="grid-container">
                  <div class="headline">Individual</div>
                  <div className="all_forms_div_wrapper">
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        First Name (Surname){" "}
                        <span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <input
                         name="First_Name"
                        onChange={(e)=>{handleInd(e.name,e)}}
                        defaultValue={""}
                          required
                          className="form_input_control"
                          type="text"
                         
                        />
                      </div>
                    </div>
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        Middle Name <span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <input
                          name="Middle_Name"
                        onChange={(e)=>{handleInd(e.name,e)}}
                        defaultValue={""}
                          required
                          className="form_input_control"
                          type="text"
                        
                        />
                      </div>
                    </div>
                  </div>
                  <div className="all_forms_div_wrapper">
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        Last Name <span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <input
                         name="Last_Name"
                        onChange={(e)=>{handleInd(e.name,e)}}
                        defaultValue={""}
                          required
                          className="form_input_control"
                          type="text"
                         
                        />
                      </div>
                    </div>
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        PAN Number<span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <input
                         name="PAN"
                        onChange={(e)=>{handleInd(e.name,e)}}
                        defaultValue={""}
                          required
                          className="form_input_control"
                          type="text"
                         
                        />
                      </div>
                    </div>
                  </div>
                  <div className="all_forms_div_wrapper">
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        Father Full Name <span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <input
                         name="Father"
                        onChange={(e)=>{handleInd(e.name,e)}}
                        defaultValue={""}
                          required
                          className="form_input_control"
                          type="text"
                         
                        />
                      </div>
                    </div>
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        Aadhar Number<span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <input
                         name="Aadhar"
                        onChange={(e)=>{handleInd(e.name,e)}}
                        defaultValue={""}
                          required
                          className="form_input_control"
                          type="text"
                         
                        />
                      </div>
                    </div>
                  </div>
                  <div className="all_forms_div_wrapper">
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        Do you have any Past Experience?{" "}
                        <span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <input
                          name="Experience"
                        onChange={(e)=>{handleInd(e.name,e)}}
                        defaultValue={""}
                          required
                          className=""
                          type="radio"
                          
                        />
                        <span
                          style={{ fontWeight: "semibold", fontSize: "10px" }}
                        >
                          YES
                        </span>{" "}
                        <input
                        name="Experience"
                        onChange={(e)=>{handleInd(e.name,e)}}
                        defaultValue={""} required className="" type="radio" />
                        <span
                          style={{ fontWeight: "semibold", fontSize: "10px" }}
                        >
                          NO
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="headline">Address For Official Communication</div>
                  <div className="all_forms_div_wrapper">
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        House No/Sy. No/Block No/Plot No{" "}
                        <span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <input
                         name="Communication"
                        onChange={(e)=>{handleInd(e.name,e)}}
                        defaultValue={""}
                          required
                          className="form_input_control"
                          type="text"
                         
                        />
                      </div>
                    </div>
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        Building Name <span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <input
                        name="Building_Name"
                        onChange={(e)=>{handleInd(e.name,e)}}
                        defaultValue={""}
                          required
                          className="form_input_control"
                          type="text"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="all_forms_div_wrapper">
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        Street Name <span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <input
                        name="Street"
                        onChange={(e)=>{handleInd(e.name,e)}}
                        defaultValue={""}
                          required
                          className="form_input_control"
                          type="text"
                        />
                      </div>
                    </div>
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        Locality <span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <input
                        name="Locality"
                        onChange={(e)=>{handleInd(e.name,e)}}
                        defaultValue={""}
                          required
                          className="form_input_control"
                          type="text"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="all_forms_div_wrapper">
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        Land mark <span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <input
                        name="Land"
                        onChange={(e)=>{handleInd(e.name,e)}}
                        defaultValue={""}
                          required
                          className="form_input_control"
                          type="text"
                        />
                      </div>
                    </div>
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        State <span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <select
                          id="cars"
                          name="state"
                          form="carform"
                          className="form-control"
                          onChange={(e)=>{handlestatedata(e,steate)}} 
                        >
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="">Select State</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="4">
                            ANDAMAN &amp; NICOBAR ISLANDS
                          </option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="5">ANDHRA PRADESH</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="6">ARUNACHAL PRADESH</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="7">ASSAM</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="8">BIHAR</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="9">CHANDIGARH</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="10">CHHATTISGARH</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="11">DADRA &amp; NAGAR HAVELI</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="12">DAMAN &amp; DIU</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="13">GOA</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="14">GUJARAT</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="15">HARYANA</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="16">HIMACHAL PRADESH</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="17">JAMMU &amp; KASHMIR</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="18">JHARKHAND</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="19">KARNATAKA</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="3">Kerala</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="20">LAKSHADWEEP</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="21">MADHYA PRADESH</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="2">Maharashtra</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="22">MANIPUR</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="23">MEGHALAYA</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="24">MIZORAM</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="25">NAGALAND</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="26">NCT OF DELHI</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="27">ODISHA</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="28">PUDUCHERRY</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="29">PUNJAB</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="30">RAJASTHAN</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="31">SIKKIM</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="32">TAMIL NADU</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} selected="selected" value="1">
                            Telangana
                          </option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="33">TRIPURA</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="34">UTTAR PRADESH</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="35">UTTARAKHAND</option>
                          <option name="state" onChange={(e)=>{handelState(e.target.value)}} value="36">WEST BENGAL</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div className="all_forms_div_wrapper">
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        District <span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <select
                          id="cars"
                          name="carlist"
                          form="carform"
                          className="form-control"
                        >
                          <option value="">Select District</option>
                          <option value="1">Adilabad</option>
                          <option value="9">Bhadradri Kothagudem </option>
                          <option selected="selected" value="25">
                            Hyderabad
                          </option>
                          <option value="28">Jagithyal</option>
                          <option value="6">Jangaon</option>
                          <option value="5">Jayashankar</option>
                          <option value="17">Jogulamba Gadwal</option>
                          <option value="2">Kamareddy</option>
                          <option value="27">Karimnagar</option>
                          <option value="8">Khammam</option>
                          <option value="26">Kumuram Bheem (Asifabad)</option>
                          <option value="7">Mahabubabad</option>
                          <option value="14">Mahabubnagar</option>
                          <option value="12">Mancherial</option>
                          <option value="10">Medak</option>
                          <option value="22">Medchal-Malkajgiri</option>
                          <option value="16">Nagarkurnool</option>
                          <option value="18">Nalgonda</option>
                          <option value="23">Nirmal</option>
                          <option value="31">Nizamabad</option>
                          <option value="29">Peddapalli</option>
                          <option value="30">Rajanna Sircilla</option>
                          <option value="24">Ranga Reddy</option>
                          <option value="11">Sangareddy</option>
                          <option value="13">Siddipet</option>
                          <option value="19">Suryapet</option>
                          <option value="21">Vikarabad</option>
                          <option value="15">Wanaparthy</option>
                          <option value="4">Warangal Rural </option>
                          <option value="3">Warangal Urban</option>
                          <option value="20">Yadadri Bhuvanagiri</option>
                        </select>
                      </div>
                    </div>
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        Mandal <span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <select
                          id="cars"
                          name="carlist"
                          form="carform"
                          className="form-control"
                        >
                          <option value="">Select Mandal</option>
                          <option selected="selected" value="458">
                            Amberpet
                          </option>
                          <option value="472">AMEERPET</option>
                          <option value="468">ASIF NAGAR</option>
                          <option value="462">Bahadurpura</option>
                          <option value="471">BALNAGAR</option>
                          <option value="457">BANDLAGUDA</option>
                          <option value="460">Charminar</option>
                          <option value="469">Golconda</option>
                          <option value="464">Himayatha Nagar</option>
                          <option value="467">KHAIRTABAD</option>
                          <option value="465">Maredpally</option>
                          <option value="463">Musheerabad</option>
                          <option value="461">Nampally</option>
                          <option value="459">Saidabad</option>
                          <option value="466">Secunderabad</option>
                          <option value="474">Secunderabad-Cantonment</option>
                          <option value="473">Secunderabad-Rasoolpura</option>
                          <option value="470">Shaikpet</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div className="all_forms_div_wrapper">
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        Village/City/Town{" "}
                        <span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <select
                          id="cars"
                          name="carlist"
                          form="carform"
                          className="form-control"
                        >
                          <option value="">Select Village</option>
                          <option value="20442">ADIKMET</option>

                          <option value="20443">ADIKMET (Blighted)</option>
                          <option value="20463">AHMED NAGAR</option>
                          <option value="20450">AKASH NAGAR</option>
                          <option value="20257">
                            Ambedkar Nagar(Slum+Blighted)
                          </option>
                          <option value="20432">ANJAIAH NAGAR</option>
                          <option value="20524">
                            ANJAIAH NAGAR (Tilak Nagar)
                          </option>
                          <option value="20499">ANJUMAN BADA</option>
                          <option value="20489">ANNAPURNA NAGAR</option>
                          <option value="20438">AZAMABAD</option>
                          <option value="20379">AZAMPURA BLIGHTED</option>
                          <option value="20453">AZATH NAGAR</option>
                          <option value="20456">BAPU NAGAR</option>
                          <option value="20535">BATHUKAMMAKUNTA</option>
                          <option value="20528">BHAGYA NAGAR</option>
                          <option value="20538">
                            BHARATH NAGAR (Tilak Nagar )
                          </option>
                          <option value="20464">CE COLONY</option>
                          <option value="20383">CHANCHAL GUDA BLIGHTED</option>
                          <option value="20457">CHENNA REDDY NAGAR</option>
                          <option value="20459">CPL QUATERS</option>
                          <option value="20513">CPL SINGLE QUARTERS </option>
                          <option value="20434">DAYANAND NAGAR</option>
                          <option value="20378">
                            DC PATEL COLONY BLIGHTED
                          </option>
                          <option value="20448">DD COLONY-I-blighted</option>
                          <option value="20454">DD COLONY-II</option>
                          <option value="20532">F G NAGAR</option>
                          <option value="20374">FARATH NAGAR </option>
                          <option value="20380">FARTH NAGAR BLIGHTED</option>
                          <option value="20386">GACHI BOWLI</option>
                          <option value="20428">GANDHI NAGAR</option>
                          <option value="20486">
                            GANGA NAGAR (Harajpenta)
                          </option>
                          <option value="20502">HARRAJPENTA</option>
                        </select>
                      </div>
                    </div>
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        Pin code <span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <input
                        onChange={(e)=>{handleInd(e.name,e)}}
                        defaultValue={""}
                          required
                          className="form_input_control"
                          type="text"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div class="grid-container">
                  <div class="headline"> Contact Details</div>
                  <div className="all_forms_div_wrapper">
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        Designation <span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <input
                        onChange={(e)=>{handleInd(e.name,e)}}
                        defaultValue={""}
                          required
                          className="form_input_control"
                          type="text"
                        />
                      </div>
                    </div>
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        First Name <span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <input
                        onChange={(e)=>{handleInd(e.name,e)}}
                        defaultValue={""}
                          required
                          className="form_input_control"
                          type="text"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="all_forms_div_wrapper">
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        Middle Name
                        <span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <input
                        onChange={(e)=>{handleInd(e.name,e)}}
                        defaultValue={""}
                          required
                          className="form_input_control"
                          type="text"
                        />
                      </div>
                    </div>
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        Last Name <span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <input
                        onChange={(e)=>{handleInd(e.name,e)}}
                        defaultValue={""}
                          required
                          className="form_input_control"
                          type="text"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="all_forms_div_wrapper">
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        PAN Number <span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <input
                        onChange={(e)=>{handleInd(e.name,e)}}
                        defaultValue={""}
                          required
                          className="form_input_control"
                          type="text"
                        />
                      </div>
                    </div>
                    <div className="details_fill_wrapper">
                      <p className="ot_text">
                        Aadhar Number <span style={{ color: "red" }}>*</span>
                      </p>
                      <div className="input_div_wrapper">
                        <input
                        onChange={(e)=>{handleInd(e.name,e)}}
                        defaultValue={""}
                          required
                          className="form_input_control"
                          type="text"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <center>
                  <input
                  onClick={()=>{handelpost()}}
                  defaultValue={""}
                    required
                    type="submit"
                    style={{
                      width: "120px",
                      height: "40px",
                      backgroundColor: "#5bc0de",
                      borderColor: "1px solid #5bc0de",
                      borderRadius: "2px",
                      color: "white",
                      fontSize: "14px",
                    }}
                  />
                </center>
              </form>
              <table className="tbl_memberData">
                <tbody>
                  <tr className="heading_row">
                    <th>First Name</th>
                    <th>Middle Name</th>
                    <th>Last Name</th>
                    <th>Designation</th>
                    <th>PAN Number</th>
                    <th>Action</th>
                  </tr>
                {console.log(formdata)}
                 {formdata?.map((member) => (
                    <tr key={member.id} className="row_value_text">
                      <td>{member.First_Name}</td>
                      <td>{member.Middle_Name}</td>
                      <td>{member.Last_Name}</td>
                      <td className="DesiValue">{member.Building_Name}</td>
                      <td>{member.PAN}</td>
                      <td>
                        <img
                          src="../Images/Edit.jpg"
                          onClick={() => handleEdit(member.id)}
                          width="20px"
                          height="20px"
                          alt="Edit"
                        />
                        {" | "}
                        <img
                          src="../Images/delete.png"
                          onClick={() => handleDelete(member.id)}
                          width="20px"
                          height="20px"
                          alt="Delete"
                        />
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </>
          )}
        </Typography>
        <Typography
          paragraph
          sx={{ p: 2, backgroundColor: "#f6f7fb" }}
        ></Typography>
      </Box>
    </Box>
  );
}
