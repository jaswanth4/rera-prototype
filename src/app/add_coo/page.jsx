'use client'
import * as React from "react";
import { styled, useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import MuiDrawer from "@mui/material/Drawer";
import MuiAppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import InboxIcon from "@mui/icons-material/MoveToInbox";
import MailIcon from "@mui/icons-material/Mail";
import profile_img from "../profilepic/JR_NTR9.png";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import Collapse from "@mui/material/Collapse";
import StarBorder from "@mui/icons-material/StarBorder";
import "../sidebar/sidebar.css";
import upload_pic from "../profilepic/img-Photo.jpg";
import Image from "next/image";

import { useRouter } from "next/navigation";
import { useEffect } from "react";

const drawerWidth = 240;

const openedMixin = (theme) => ({
  width: drawerWidth,
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: "hidden",
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: "hidden",
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up("sm")]: {
    width: `calc(${theme.spacing(8)} + 1px)`,
  },
});

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  ...(open && {
    ...openedMixin(theme),
    "& .MuiDrawer-paper": openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme),
  }),
}));

const data = [
  {
    id: 56354,
    firstName: "NAVANEETH",
    middleName: "KUMAR",
    lastName: "NARISETTY",
    designation: "Proprietor",
    panNumber: "xxxxxx574E",
  },
  {
    id: 56355,
    firstName: "NAVANEETH",
    middleName: "KUMAR",
    lastName: "NARISETTY",
    designation: "Authorized Signatory",
    panNumber: "xxxxxx574E",
  },
  // Add more data as needed
];
export default function MiniDrawer() {
  const theme = useTheme();
  const [open, setOpen] = React.useState(true);
  const [image, setImage] = React.useState(null);
  const [checked, setchecked] = React.useState(null);
  const [steate,handelState]=React.useState(false)
  const router = useRouter();
  const [formdata,setFormdata]=React.useState([])
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handelpost=()=>{

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(obj)
  };
  fetch('http://localhost:3004/profile', requestOptions)
      .then(response => alert("Form submited "))
      
}

  const handleDrawerClose = () => {
    setOpen(false);
  };
  const [nested, setNested] = React.useState(false);
  const [nested2, setNested2] = React.useState(false);

  const handleClick = () => {
    setNested(!nested);
  };
  const handleClick2 = () => {
    setNested2(!nested2);
  };

  const handleImageChange = (e) => {
    const file = e.target.files[0];

    if (file) {
      const reader = new FileReader();

      reader.onload = (event) => {
        setImage(event.target.result);
      };

      reader.readAsDataURL(file);
    } else {
      setImage(null);
    }
  };

  const handleEdit = (id) => {
    // Handle edit action
    console.log(`Edit member with id ${id}`);
  };

  const handleDelete = (id) => {
    // Handle delete action
    console.log(`Delete member with id ${id}`);
  };
  var obj={}
  const handleInd=(name,e)=>{
    obj[e.target.name]=e.target.value
    console.log(e)
  
  }
  const handlestatedata=(vale,val)=>{
 
  }
 useEffect(()=>{

  fetch('http://localhost:3004/projects')
  .then(response => response.json())
  .then(data => setFormdata(data));
      

 },[])
 const [selectedOption, setSelectedOption] = React.useState('');

 const handleSelectChange = (e) => {
   // Update the state with the selected option value

   setSelectedOption(e.target.value);
   router.push("../project")
 };

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <div>
      <Drawer variant="permanent" open={open}>
          {open ? (
            <center>
              <div className="profile_pic">
                <a href="#" class="site_title">
                  {" "}
                  <span>Telangana RERA</span>
                </a>

                <img src="https://avada.com/wp-content/uploads/2018/11/person_sample_2.jpg" />
                <p>Welcome</p>
                <h5>Priya Gudipalli</h5>
                <p>Promoter</p>
              </div>
            </center>
          ) : null}

          <List sx={{ backgroundColor: "#272d33", color: "white" }}>
            <ListItemButton onClick={handleClick}>
              <ListItemText primary="Account" />
              {nested ? <ExpandLess /> : <ExpandMore />}
            </ListItemButton>
            <Collapse in={nested} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                <ListItemButton sx={{ pl: 4 }}>
                  <ListItemText
                    onClick={() => {
                      router.push("./profile");
                    }}
                    primary=" My Profile"
                  />
                </ListItemButton>
                <ListItemButton sx={{ pl: 4 }}>
                  <ListItemText
                    onClick={() => {
                      router.push("./Organizations");
                    }}
                    primary=" Add Organizations Other Member "
                  />
                </ListItemButton>
                <ListItemButton sx={{ pl: 4 }}>
                  <ListItemText
                    onClick={() => {
                      router.push("./Experience");
                    }}
                    primary=" Past Experience Details"
                  />
                </ListItemButton>
                <ListItemButton sx={{ pl: 4 }}>
                  <ListItemText primary="Change Password" />
                </ListItemButton>
              </List>
            </Collapse>
          </List>

          <List sx={{ backgroundColor: "#272d33", color: "white" }}>
            <ListItemButton onClick={handleClick2}>
              <ListItemText primary="Project Details" />
              {nested2 ? <ExpandLess /> : <ExpandMore />}
            </ListItemButton>
            <Collapse in={nested2} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                {[
                  { name: "Add Project", path: "../project" },
                  { name: "Add Co-Promoter Details", path: "../add_coo" },
                  { name: "Add Buildings", path: "../project" },
                  { name: "Common Areas and Facilities", path: "../project" },
                  { name: "Add Project Cost", path: "../project" },
                  {
                    name: "Add Project Professional Details",
                    path: "../project",
                  },
                  { name: "Document Upload", path: "../project" },
                  {
                    name: "Add Litigations Related to the Project",
                    path: "../project",
                  },
                  { name: "Task/Activity", path: "../project" },
                  { name: "Application Withdrawal", path: "../project" },
                  { name: "Application For Change", path: "../project" },
                  { name: "Project Extension", path: "../project" },
                  { name: "Download Payment Receipts", path: "../project" },
                ].map((name) => (
                  <ListItemButton sx={{ pl: 4 }}>
                    <ListItemText
                      onClick={() => {
                        router.push(name.path);
                      }}
                      primary={name.name}
                    />
                  </ListItemButton>
                ))}
              </List>
            </Collapse>
          </List>
          <List sx={{ backgroundColor: "#272d33", color: "white" }}>
            <ListItemButton onClick={handleClick2}>
              <ListItemText primary="Payment " />
            </ListItemButton>
            <Collapse in={nested2} timeout="auto" unmountOnExit></Collapse>
          </List>
          <List sx={{ backgroundColor: "#272d33", color: "white" }}>
            <ListItemButton onClick={handleClick2}>
              <ListItemText primary="Extantion " />
            </ListItemButton>
            <Collapse in={nested2} timeout="auto" unmountOnExit></Collapse>
          </List>
          <List sx={{ backgroundColor: "#272d33", color: "white" }}>
            <ListItemButton onClick={handleClick2}>
              <ListItemText primary="Download Payment recipt " />
            </ListItemButton>
            <Collapse in={nested2} timeout="auto" unmountOnExit></Collapse>
          </List>
          <List sx={{ backgroundColor: "#272d33", color: "white" }}>
            <ListItemButton onClick={handleClick2}>
              <ListItemText primary="Logout " />
            </ListItemButton>
            <Collapse in={nested2} timeout="auto" unmountOnExit></Collapse>
          </List>
        </Drawer>
      </div>
      <Box component="main" sx={{ flexGrow: 1, backgroundColor: "#f6f7fb" }}>
        <Toolbar sx={{ backgroundColor: "white" }}>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{
              marginRight: 5,
              ...(open && { display: "none" }),
            }}
          >
            <MenuIcon />
          </IconButton>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerClose}
            edge="start"
            sx={{
              marginRight: 5,
              ...(!open && { display: "none" }),
            }}
          >
            <MenuIcon />
          </IconButton>

          <div>
            <img
              className="logo_img"
              src="https://rerait.telangana.gov.in/Images/rera%20logo.png"
            />
          </div>

          <Typography variant="h6" noWrap component="div"></Typography>
          <h3 className="header">
            Telangana State Real Estate Regulatory Authority
          </h3>
        </Toolbar>
        <Typography variant="h6" noWrap component="div">
        <div class="grid-container">
            <div class="headline">Add Co-Promoter Details</div>
            <br></br>
            <div class="headline">
              <span
                style={{ color: "red", fontSize: "13px", fontWeight: "bold" }}
              >
                *All * Mark field are mandatory.
              </span>
            </div>
            <br></br>
            <div class="headline">Project Name</div>
            <div className="all_forms_div_wrapper">
              <div className="details_fill_wrapper">
                <p className="ot_text">
                Project Name <span style={{ color: "white" }}>*</span>
                </p>
                <div className="input_div_wrapper">
                 
                <select
                 value={selectedOption}
                 onChange={handleSelectChange}
                        id="cars"
                        name="carlist"
                        form="carform"
                        className="form-control"
                      >  <option value="5">{""}</option>
                        {
                       
                        formdata?.map((name)=>(
                            <option  value="5">{name.projectname}</option>
                        ))
                      }
                       
                     
                      </select>
                  
                </div>
              </div>
              {/* <div className="details_fill_wrapper">
                <p className="ot_text">
                  Name <span style={{ color: "red" }}>*</span>
                </p>
                <div className="input_div_wrapper">
                  <input
                  onChange={(e)=>{handleInd(e.name,e)}}
                  defaultValue={""}
                  required
                          className="form_input_control"
                    type="text"
                  />
                </div>
              </div> */}
            </div>
          </div>
        </Typography>
    

        
      </Box>
    </Box>
  );
}
