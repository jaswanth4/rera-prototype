'use client'
import * as React from "react";
import { styled, useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import MuiDrawer from "@mui/material/Drawer";
import MuiAppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import InboxIcon from "@mui/icons-material/MoveToInbox";
import MailIcon from "@mui/icons-material/Mail";
import profile_img from "../profilepic/JR_NTR9.png";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import Collapse from "@mui/material/Collapse";
import StarBorder from "@mui/icons-material/StarBorder";
import "../sidebar/sidebar.css";
import upload_pic from "../profilepic/img-Photo.jpg";
import Image from "next/image";


import { useRouter } from 'next/navigation'
import { useEffect } from "react";

const drawerWidth = 240;

const openedMixin = (theme) => ({
    width: drawerWidth,
    transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
    }),
    overflowX: "hidden",
});

const closedMixin = (theme) => ({
    transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: `calc(${theme.spacing(7)} + 1px)`,
    [theme.breakpoints.up("sm")]: {
        width: `calc(${theme.spacing(8)} + 1px)`,
    },
});

const DrawerHeader = styled("div")(({ theme }) => ({
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
}));

const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(["width", "margin"], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    }),
}));

const Drawer = styled(MuiDrawer, {
    shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
    boxSizing: "border-box",
    ...(open && {
        ...openedMixin(theme),
        "& .MuiDrawer-paper": openedMixin(theme),
    }),
    ...(!open && {
        ...closedMixin(theme),
        "& .MuiDrawer-paper": closedMixin(theme),
    }),
}));


const data = [
    {
        id: 1,
        name: "YASODHATRI PHARMA TOWNSHIP",
        address: "24/B 24/B3/2, Challampally to Thakraj Guda Road",

        BoundariesEast: { surveyNumber: "Sy No 26", owner: "Butchi Reddy Land" },
        BoundariesWest: { surveyNumber: "Sy No 24", owner: "Shekar Reddy Land" },
       BoundariesNorth : { surveyNumber: "Sy No 24", owner: "Lokya Land" },
       BoundariesSouth : { surveyNumber: "Sy No 24", owner: "Lokya Land" },

        area: 12140.57,
        actions: ["Edit", "Delete"],
    },


    // Add more data as needed
];
export default function MiniDrawer() {
    const theme = useTheme();
    const [open, setOpen] = React.useState(true);
        const [image, setImage] = React.useState(null);
    const [checked, setchecked] = React.useState(null)
    const [formdata,setFormdata]=React.useState(null)
    const [selectedState, setSelectedState] =React.useState('');
    const router = useRouter()
    const handleStateChange = (e) => {
        const selectedValue = e.target.value;
        setSelectedState(selectedValue);
        // You can perform additional actions here based on the selected state
      };
    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };
    const [nested, setNested] = React.useState(false);
    const [nested2, setNested2] = React.useState(false);

    const handleClick = () => {
        setNested(!nested);
    };
    const handleClick2 = () => {
        setNested2(!nested2);
    };

    const handleImageChange = (e) => {
        const file = e.target.files[0];

        if (file) {
            const reader = new FileReader();

            reader.onload = (event) => {
                setImage(event.target.result);
            };

            reader.readAsDataURL(file);
        } else {
            setImage(null);
        }
    };


    const handleEdit = (id) => {
        // Handle edit action
        console.log(`Edit member with id ${id}`);
    };

    const handleDelete = (id) => {
        // Handle delete action
        console.log(`Delete member with id ${id}`);
    };
    var obj={}
    const handleInd=(name,e)=>{
      obj[e.target.name]=e.target.value
      console.log(e)
    
    }
    const handlestatedata=(vale,val)=>{
   
    }
   useEffect(()=>{
  
    fetch('http://localhost:3004/projects')
    .then(response => response.json())
    .then(data => setFormdata(data));
        
   },[])
   const handelpost=()=>{

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(obj)
  };
  fetch('http://localhost:3004/projects', requestOptions)
      .then(response =>{console.log("hi")})
      
}

    return (
        <Box sx={{ display: "flex" }}>
            <CssBaseline />
            <div>
            <Drawer variant="permanent" open={open}>
          {open ? (
            <center>
              <div className="profile_pic">
                <a href="#" class="site_title">
                  {" "}
                  <span>Telangana RERA</span>
                </a>

                <img src="https://avada.com/wp-content/uploads/2018/11/person_sample_2.jpg" />
                <p>Welcome</p>
                <h5>Priya Gudipalli</h5>
                <p>Promoter</p>
              </div>
            </center>
          ) : null}

          <List sx={{ backgroundColor: "#272d33", color: "white" }}>
            <ListItemButton onClick={handleClick}>
              <ListItemText primary="Account" />
              {nested ? <ExpandLess /> : <ExpandMore />}
            </ListItemButton>
            <Collapse in={nested} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                <ListItemButton sx={{ pl: 4 }}>
                  <ListItemText
                    onClick={() => {
                      router.push("./profile");
                    }}
                    primary=" My Profile"
                  />
                </ListItemButton>
                <ListItemButton sx={{ pl: 4 }}>
                  <ListItemText
                    onClick={() => {
                      router.push("./Organizations");
                    }}
                    primary=" Add Organizations Other Member "
                  />
                </ListItemButton>
                <ListItemButton sx={{ pl: 4 }}>
                  <ListItemText
                    onClick={() => {
                      router.push("./Experience");
                    }}
                    primary=" Past Experience Details"
                  />
                </ListItemButton>
                <ListItemButton sx={{ pl: 4 }}>
                  <ListItemText primary="Change Password" />
                </ListItemButton>
              </List>
            </Collapse>
          </List>

          <List sx={{ backgroundColor: "#272d33", color: "white" }}>
            <ListItemButton onClick={handleClick2}>
              <ListItemText primary="Project Details" />
              {nested2 ? <ExpandLess /> : <ExpandMore />}
            </ListItemButton>
            <Collapse in={nested2} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                {[
                  { name: "Add Project", path: "../project" },
                  { name: "Add Co-Promoter Details", path: "../add_coo" },
                  { name: "Add Buildings", path: "../project" },
                  { name: "Common Areas and Facilities", path: "../project" },
                  { name: "Add Project Cost", path: "../project" },
                  {
                    name: "Add Project Professional Details",
                    path: "../project",
                  },
                  { name: "Document Upload", path: "../project" },
                  {
                    name: "Add Litigations Related to the Project",
                    path: "../project",
                  },
                  { name: "Task/Activity", path: "../project" },
                  { name: "Application Withdrawal", path: "../project" },
                  { name: "Application For Change", path: "../project" },
                  { name: "Project Extension", path: "../project" },
                  { name: "Download Payment Receipts", path: "../project" },
                ].map((name) => (
                  <ListItemButton sx={{ pl: 4 }}>
                    <ListItemText
                      onClick={() => {
                        router.push(name.path);
                      }}
                      primary={name.name}
                    />
                  </ListItemButton>
                ))}
              </List>
            </Collapse>
          </List>
          <List sx={{ backgroundColor: "#272d33", color: "white" }}>
            <ListItemButton onClick={handleClick2}>
              <ListItemText primary="Payment " />
            </ListItemButton>
            <Collapse in={nested2} timeout="auto" unmountOnExit></Collapse>
          </List>
          <List sx={{ backgroundColor: "#272d33", color: "white" }}>
            <ListItemButton onClick={handleClick2}>
              <ListItemText primary="Extantion " />
            </ListItemButton>
            <Collapse in={nested2} timeout="auto" unmountOnExit></Collapse>
          </List>
          <List sx={{ backgroundColor: "#272d33", color: "white" }}>
            <ListItemButton onClick={handleClick2}>
              <ListItemText primary="Download Payment recipt " />
            </ListItemButton>
            <Collapse in={nested2} timeout="auto" unmountOnExit></Collapse>
          </List>
          <List sx={{ backgroundColor: "#272d33", color: "white" }}>
            <ListItemButton onClick={handleClick2}>
              <ListItemText primary="Logout " />
            </ListItemButton>
            <Collapse in={nested2} timeout="auto" unmountOnExit></Collapse>
          </List>
        </Drawer>
            </div>
            <Box component="main" sx={{ flexGrow: 1, backgroundColor: "#f6f7fb" }}>
                <Toolbar sx={{ backgroundColor: "white" }}>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        sx={{
                            marginRight: 5,
                            ...(open && { display: "none" }),
                        }}
                    >
                        <MenuIcon />
                    </IconButton>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerClose}
                        edge="start"
                        sx={{
                            marginRight: 5,
                            ...(!open && { display: "none" }),
                        }}
                    >
                        <MenuIcon />
                    </IconButton>

                    <div>
                        <img
                            className="logo_img"
                            src="https://rerait.telangana.gov.in/Images/rera%20logo.png"
                        />
                    </div>

                    <Typography variant="h6" noWrap component="div"></Typography>
                    <h3 className="header">
                        Telangana State Real Estate Regulatory Authority
                    </h3>
                </Toolbar>

                <Typography paragraph sx={{ p: 2, backgroundColor: "#f6f7fb" }}>
                    <div class="grid-container">
                        <div class="headline">Add Project</div>
                        <br></br>
                        <div class="headline">
                            <span
                                style={{ color: "red", fontSize: "13px", fontWeight: "bold" }}
                            >
                                *All * Mark field are mandatory.
                            </span>
                        </div>
                        <br></br>

                        {/* <div className="all_forms_div_wrapper">
                           
                            {/* <div className="details_fill_wrapper">
                <p className="ot_text">
                  Name <span style={{ color: "red" }}>*</span>
                </p>
                <div className="input_div_wrapper">
                  <input
                  required

                    defaultValue={"Vinay Kumar"}
                    className="form_input_control"
                    type="text"
                  />
                </div>
              </div> 
                        </div>
                         */}
                    </div>
                    {/* {checked ?
                         : <>
                            <div class="grid-container">


                                <div class="headline">Individual
                                </div>
                                <div className="all_forms_div_wrapper">
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            First Name (Surname) <span style={{ color: "white" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <input
                                            required

                                                defaultValue={"Vinay Kumar"}
                                                className="form_input_control"
                                                type="text"
                                            />
                                            <span style={{ color: "red" }}>*</span>
                                        </div>
                                    </div>
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            Middle Name <span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <input
                                            required

                                                defaultValue={"Vinay Kumar"}
                                                className="form_input_control"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="all_forms_div_wrapper">
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            Last Name <span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <input
                                            required

                                                defaultValue={"Last Name"}
                                                className="form_input_control"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            PAN Number<span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <input
                                            required

                                                defaultValue={"PAN Number"}
                                                className="form_input_control"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="all_forms_div_wrapper">
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            Father Full Name <span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <input
                                            required

                                                defaultValue={"Father Full Name"}
                                                className="form_input_control"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            Aadhar Number<span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <input
                                            required

                                                defaultValue={"Aadhar Number"}
                                                className="form_input_control"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="all_forms_div_wrapper">
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            Do you have any Past Experience?{" "}
                                            <span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <input
                                            required
                                             className="" type="radio" />
                                            <span style={{ fontWeight: "semibold", fontSize: "10px" }}>
                                                YES
                                            </span>{" "}
                                            <input
                                            required
                                             className="" type="radio" />
                                            <span style={{ fontWeight: "semibold", fontSize: "10px" }}>
                                                NO
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="headline">Address For Official Communication
                                </div>
                                <div className="all_forms_div_wrapper">
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            House No/Sy. No/Block No/Plot No{" "}
                                            <span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <input
                                            required

                                                defaultValue={"22 A- 33 44 N"}
                                                className="form_input_control"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            Building Name <span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <input
                                            required

                                                defaultValue={"Building Name"}
                                                className="form_input_control"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="all_forms_div_wrapper">
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            Street Name <span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <input
                                            required

                                                defaultValue={""}
                                                className="form_input_control"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            Locality <span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <input
                                            required

                                                defaultValue={"Locality"}
                                                className="form_input_control"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="all_forms_div_wrapper">
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            Land mark <span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <input
                                            required

                                                defaultValue={""}
                                                className="form_input_control"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            State <span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <select
                                                id="cars"
                                                name="carlist"
                                                form="carform"
                                                className="form-control"
                                            >
                                                <option value="">Select State</option>
                                                <option value="4">ANDAMAN &amp; NICOBAR ISLANDS</option>
                                                <option value="5">ANDHRA PRADESH</option>
                                                <option value="6">ARUNACHAL PRADESH</option>
                                                <option value="7">ASSAM</option>
                                                <option value="8">BIHAR</option>
                                                <option value="9">CHANDIGARH</option>
                                                <option value="10">CHHATTISGARH</option>
                                                <option value="11">DADRA &amp; NAGAR HAVELI</option>
                                                <option value="12">DAMAN &amp; DIU</option>
                                                <option value="13">GOA</option>
                                                <option value="14">GUJARAT</option>
                                                <option value="15">HARYANA</option>
                                                <option value="16">HIMACHAL PRADESH</option>
                                                <option value="17">JAMMU &amp; KASHMIR</option>
                                                <option value="18">JHARKHAND</option>
                                                <option value="19">KARNATAKA</option>
                                                <option value="3">Kerala</option>
                                                <option value="20">LAKSHADWEEP</option>
                                                <option value="21">MADHYA PRADESH</option>
                                                <option value="2">Maharashtra</option>
                                                <option value="22">MANIPUR</option>
                                                <option value="23">MEGHALAYA</option>
                                                <option value="24">MIZORAM</option>
                                                <option value="25">NAGALAND</option>
                                                <option value="26">NCT OF DELHI</option>
                                                <option value="27">ODISHA</option>
                                                <option value="28">PUDUCHERRY</option>
                                                <option value="29">PUNJAB</option>
                                                <option value="30">RAJASTHAN</option>
                                                <option value="31">SIKKIM</option>
                                                <option value="32">TAMIL NADU</option>
                                                <option selected="selected" value="1">
                                                    Telangana
                                                </option>
                                                <option value="33">TRIPURA</option>
                                                <option value="34">UTTAR PRADESH</option>
                                                <option value="35">UTTARAKHAND</option>
                                                <option value="36">WEST BENGAL</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="all_forms_div_wrapper">
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            District <span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <select
                                                id="cars"
                                                name="carlist"
                                                form="carform"
                                                className="form-control"
                                            >
                                                <option value="">Select District</option>
                                                <option value="1">Adilabad</option>
                                                <option value="9">Bhadradri Kothagudem </option>
                                                <option selected="selected" value="25">
                                                    Hyderabad
                                                </option>
                                                <option value="28">Jagithyal</option>
                                                <option value="6">Jangaon</option>
                                                <option value="5">Jayashankar</option>
                                                <option value="17">Jogulamba Gadwal</option>
                                                <option value="2">Kamareddy</option>
                                                <option value="27">Karimnagar</option>
                                                <option value="8">Khammam</option>
                                                <option value="26">Kumuram Bheem (Asifabad)</option>
                                                <option value="7">Mahabubabad</option>
                                                <option value="14">Mahabubnagar</option>
                                                <option value="12">Mancherial</option>
                                                <option value="10">Medak</option>
                                                <option value="22">Medchal-Malkajgiri</option>
                                                <option value="16">Nagarkurnool</option>
                                                <option value="18">Nalgonda</option>
                                                <option value="23">Nirmal</option>
                                                <option value="31">Nizamabad</option>
                                                <option value="29">Peddapalli</option>
                                                <option value="30">Rajanna Sircilla</option>
                                                <option value="24">Ranga Reddy</option>
                                                <option value="11">Sangareddy</option>
                                                <option value="13">Siddipet</option>
                                                <option value="19">Suryapet</option>
                                                <option value="21">Vikarabad</option>
                                                <option value="15">Wanaparthy</option>
                                                <option value="4">Warangal Rural </option>
                                                <option value="3">Warangal Urban</option>
                                                <option value="20">Yadadri Bhuvanagiri</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            Mandal <span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <select
                                                id="cars"
                                                name="carlist"
                                                form="carform"
                                                className="form-control"
                                            >
                                                <option value="">Select Mandal</option>
                                                <option selected="selected" value="458">
                                                    Amberpet
                                                </option>
                                                <option value="472">AMEERPET</option>
                                                <option value="468">ASIF NAGAR</option>
                                                <option value="462">Bahadurpura</option>
                                                <option value="471">BALNAGAR</option>
                                                <option value="457">BANDLAGUDA</option>
                                                <option value="460">Charminar</option>
                                                <option value="469">Golconda</option>
                                                <option value="464">Himayatha Nagar</option>
                                                <option value="467">KHAIRTABAD</option>
                                                <option value="465">Maredpally</option>
                                                <option value="463">Musheerabad</option>
                                                <option value="461">Nampally</option>
                                                <option value="459">Saidabad</option>
                                                <option value="466">Secunderabad</option>
                                                <option value="474">Secunderabad-Cantonment</option>
                                                <option value="473">Secunderabad-Rasoolpura</option>
                                                <option value="470">Shaikpet</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="all_forms_div_wrapper">
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            Village/City/Town <span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <select
                                                id="cars"
                                                name="carlist"
                                                form="carform"
                                                className="form-control"
                                            >
                                                <option value="">Select Village</option>
                                                <option value="20442">ADIKMET</option>

                                                <option value="20443">ADIKMET (Blighted)</option>
                                                <option value="20463">AHMED NAGAR</option>
                                                <option value="20450">AKASH NAGAR</option>
                                                <option value="20257">Ambedkar Nagar(Slum+Blighted)</option>
                                                <option value="20432">ANJAIAH NAGAR</option>
                                                <option value="20524">ANJAIAH NAGAR (Tilak Nagar)</option>
                                                <option value="20499">ANJUMAN BADA</option>
                                                <option value="20489">ANNAPURNA NAGAR</option>
                                                <option value="20438">AZAMABAD</option>
                                                <option value="20379">AZAMPURA BLIGHTED</option>
                                                <option value="20453">AZATH NAGAR</option>
                                                <option value="20456">BAPU NAGAR</option>
                                                <option value="20535">BATHUKAMMAKUNTA</option>
                                                <option value="20528">BHAGYA NAGAR</option>
                                                <option value="20538">BHARATH NAGAR (Tilak Nagar )</option>
                                                <option value="20464">CE COLONY</option>
                                                <option value="20383">CHANCHAL GUDA BLIGHTED</option>
                                                <option value="20457">CHENNA REDDY NAGAR</option>
                                                <option value="20459">CPL QUATERS</option>
                                                <option value="20513">CPL SINGLE QUARTERS </option>
                                                <option value="20434">DAYANAND NAGAR</option>
                                                <option value="20378">DC PATEL COLONY BLIGHTED</option>
                                                <option value="20448">DD COLONY-I-blighted</option>
                                                <option value="20454">DD COLONY-II</option>
                                                <option value="20532">F G NAGAR</option>
                                                <option value="20374">FARATH NAGAR </option>
                                                <option value="20380">FARTH NAGAR BLIGHTED</option>
                                                <option value="20386">GACHI BOWLI</option>
                                                <option value="20428">GANDHI NAGAR</option>
                                                <option value="20486">GANGA NAGAR (Harajpenta)</option>
                                                <option value="20502">HARRAJPENTA</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            Pin code <span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <input
                                            required

                                                defaultValue={534003}
                                                className="form_input_control"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="grid-container">
                                <div class="headline"> Contact Details</div>
                                <div className="all_forms_div_wrapper">
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            Designation <span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <input
                                            required

                                                defaultValue={"Designation "}
                                                className="form_input_control"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            First Name <span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <input
                                            required

                                                defaultValue={"First Name"}
                                                className="form_input_control"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="all_forms_div_wrapper">
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            Middle Name
                                            <span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <input
                                            required

                                                defaultValue={"Middle Name"}
                                                className="form_input_control"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            Last Name <span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <input
                                            required

                                                defaultValue={"Last Name"}
                                                className="form_input_control"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="all_forms_div_wrapper">
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            PAN Number <span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <input
                                            required

                                                defaultValue={"XXXXX78688"}
                                                className="form_input_control"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                    <div className="details_fill_wrapper">
                                        <p className="ot_text">
                                            Aadhar Number <span style={{ color: "red" }}>*</span>
                                        </p>
                                        <div className="input_div_wrapper">
                                            <input
                                            required

                                                defaultValue={"Aadhar Number "}
                                                className="form_input_control"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <center>
                                <button style={{ width: "120px", height: "40px", backgroundColor: "#5bc0de", borderColor: "1px solid #5bc0de", borderRadius: "2px", color: "white", fontSize: "14px" }}>Save profile</button>
                            </center>
                            <table className="tbl_memberData" >
                                <tbody>
                                    <tr className="heading_row">
                                        <th >First Name</th>
                                        <th>Middle Name</th>
                                        <th>Last Name</th>
                                        <th>Designation</th>
                                        <th>PAN Number</th>
                                        <th>Action</th>
                                    </tr>
                                    {data.map((member) => (
                                        <tr key={member.id} className="row_value_text">
                                            <td >{member.firstName}</td>
                                            <td>{member.middleName}</td>
                                            <td>{member.lastName}</td>
                                            <td className="DesiValue">{member.designation}</td>
                                            <td>{member.panNumber}</td>
                                            <td>
                                                <img
                                                    src="../Images/Edit.jpg"
                                                    onClick={() => handleEdit(member.id)}
                                                    width="20px"
                                                    height="20px"
                                                    alt="Edit"
                                                />
                                                {' | '}
                                                <img
                                                    src="../Images/delete.png"
                                                    onClick={() => handleDelete(member.id)}
                                                    width="20px"
                                                    height="20px"
                                                    alt="Delete"
                                                />
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table></>} */}
                    <>
                    <form>
                        <div class="grid-container">


                            <div class="headline">
                                Project Information
                            </div>
                            <div className="all_forms_div_wrapper">
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Authority Name <span style={{ color: "white" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                
                                        <select
                                        onChange={handleStateChange} 
                                        value={selectedState}
                                            id="cars"
                                            name="carlist"
                                            form="carform"
                                            className="form-control"
                                        >
                                            <option value="5">Select</option>
                                            <option value="5">DTCP</option>
                                            <option value="6">HMDA</option>
                                            <option value="7">GHMC</option>


                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div className="all_forms_div_wrapper">
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Plan Approval Number <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required

                                            defaultValue={"Plan Approval Number"}
                                            className="form_input_control"
                                            type="text"
                                        />
                                        {/* <button style={{ width: "120px", height: "40px", backgroundColor: "#5bc0de", borderColor: "#5bc0de", borderRadius: "10px", color: "white", fontSize: "14px", }}>Check</button> */}

                                    </div>

                                </div>

                            </div>
                            <div className="all_forms_div_wrapper">
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Project Status?{" "}
                                        <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required
                                         className="" type="radio" />
                                        <span style={{ fontWeight: "semibold", fontSize: "14px" }}>
                                            New Project
                                        </span>{" "}
                                        <input
                                        required
                                         className="" type="radio" />
                                        <span style={{ fontWeight: "semibold", fontSize: "14px" }}>

                                            On-Going Project

                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div className="all_forms_div_wrapper">
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Approved Date <span style={{ color: "white" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required
                                         type="date"
                                            className="form_input_control" />
                                    </div>
                                </div>

                            </div>
                            <div className="all_forms_div_wrapper">
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Project Name <span style={{ color: "white" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required
                                         type="text"
                                      
                                        name="projectname"
                                            onChange={(e)=>{{handleInd(e,e)}}}
                                            className="form_input_control" />
                                    </div>
                                </div>
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Project type <span style={{ color: "white" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <select id="cars"
                                            name="carlist"

                                            className="form-control"><option value="">Select Type</option>
                                            <option value="12">Commercial</option>
                                            <option value="13">Residential</option>
                                            <option value="15">Plotted Development</option>
                                            <option value="33">Mixed Development (Residential &amp; Commercial)</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div className="all_forms_div_wrapper">
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Proposed Date of Completion <span style={{ color: "white" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required
                                         type="date"
                                            className="form_input_control" />
                                    </div>
                                </div>
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Revised Proposed Date of Completion <span style={{ color: "white" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required
                                         type="date"
                                            className="form_input_control" />
                                    </div>
                                </div>

                            </div>
                            <div className="all_forms_div_wrapper">
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Litigations related to the project ?<span style={{ color: "white" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <div className="input_div_wrapper">
                                            <input
                                            required
                                             className="" type="radio" />
                                            <span style={{ fontWeight: "semibold", fontSize: "14px" }}>
                                                YES
                                            </span>{" "}
                                            <input
                                          
                                             className="" type="radio" />
                                            <span style={{ fontWeight: "semibold", fontSize: "14px" }}>

                                                NO
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Are there any Promoter(Land Owner/ Investor)<br></br> (as defined by Telangana RERA Order) in the project ? <span style={{ color: "white" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <div className="input_div_wrapper">
                                            <input
                                            required
                                             className="" type="radio" />
                                            <span style={{ fontWeight: "semibold", fontSize: "14px" }}>
                                                YES
                                            </span>{" "}
                                            <input
                                                 className="" type="radio" />
                                            <span style={{ fontWeight: "semibold", fontSize: "14px" }}>

                                                NO
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>






                        </div>
                        <div class="grid-container">
                            <div class="headline">Land Details</div>
                            <div className="all_forms_div_wrapper">
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Sy.No/TS No. <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required

                                            defaultValue={"Sy.No/TS No. "}
                                            className="form_input_control"
                                            type="text"
                                        />
                                    </div>
                                </div>
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Plot No./House No. <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required

                                            defaultValue={"First Name"}
                                            className="form_input_control"
                                            type="text"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="all_forms_div_wrapper">
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Total Area(In sqmts)
                                        <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required

                                            defaultValue={"Total Area(In sqmts) "}
                                            className="form_input_control"
                                            type="text"
                                        />
                                    </div>
                                </div>
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Area affected Road widening/FTL <br></br>of Tanks/Nala Widening(In sqmts) <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required

                                            defaultValue={"Total Area(In sqmts)"}
                                            className="form_input_control"
                                            type="text"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="all_forms_div_wrapper">
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Net Area(In sqmts) <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required

                                            defaultValue={"Total Area(In sqmts)"}
                                            className="form_input_control"
                                            type="text"
                                        />
                                    </div>
                                </div>
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Total Building Units (as per approved plan) <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required

                                            defaultValue={"Net Area(In sqmts) "}
                                            className="form_input_control"
                                            type="text"
                                        />
                                    </div>
                                </div>
                            </div>

                            <div className="all_forms_div_wrapper">
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Proposed Building Units(as per agreement) <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <select
                                            id="cars"
                                            name="carlist"
                                            form="carform"
                                            className="form-control">
                                            {Array.from({ length: 1500 }, (_, index) => index + 1).map((i) => (

                                                <option>{i}</option>
                                            ))}

                                        </select>


                                    </div>
                                </div>

                            </div>
                            <div className="all_forms_div_wrapper">
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Boundaries East <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required

                                     
                                        name="East"
                                            onChange={(e)=>{{handleInd(e,e)}}}
                                            className="form_input_control"
                                            defaultValue={""}
                                            type="text"
                                        />
                                    </div>
                                </div>
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Boundaries West<span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required

                                        name="west"
                                            onChange={(e)=>{{handleInd(e,e)}}}
                                            className="form_input_control"
                                            defaultValue={""}
                                            type="text"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="all_forms_div_wrapper">
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Boundaries South <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required

                                        name="south"
                                            onChange={(e)=>{{handleInd(e,e)}}}
                                            className="form_input_control"
                                            defaultValue={""}
                                            type="text"
                                        />
                                    </div>
                                </div>
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">

                                        Boundaries North
                                        <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required

                                  
                                        name="north"
                                            onChange={(e)=>{{handleInd(e,e)}}}
                                            className="form_input_control"
                                            defaultValue={""}
                                            type="text"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div class="headline">Built-Up Area Details</div>
                            <div className="all_forms_div_wrapper">
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Approved Built up Area (In Sqmts) <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required

                                      
                                        name="area"
                                            onChange={(e)=>{{handleInd(e,e)}}}
                                          
                                            defaultValue={""}
                                            className="form_input_control"
                                            type="text"
                                        />
                                    </div>
                                </div>
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Mortgage Area (In Sqmts)   <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required

                                            defaultValue={"First Name"}
                                            className="form_input_control"
                                            type="text"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div class="headline">Address Details</div>
                            <div className="all_forms_div_wrapper">

                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        State <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <select
                                            id="cars"
                                            name="carlist"
                                            form="carform"
                                            className="form-control"
                                        >
                                            <option value="">Select State</option>
                                            <option value="4">ANDAMAN &amp; NICOBAR ISLANDS</option>
                                            <option value="5">ANDHRA PRADESH</option>
                                            <option value="6">ARUNACHAL PRADESH</option>
                                            <option value="7">ASSAM</option>
                                            <option value="8">BIHAR</option>
                                            <option value="9">CHANDIGARH</option>
                                            <option value="10">CHHATTISGARH</option>
                                            <option value="11">DADRA &amp; NAGAR HAVELI</option>
                                            <option value="12">DAMAN &amp; DIU</option>
                                            <option value="13">GOA</option>
                                            <option value="14">GUJARAT</option>
                                            <option value="15">HARYANA</option>
                                            <option value="16">HIMACHAL PRADESH</option>
                                            <option value="17">JAMMU &amp; KASHMIR</option>
                                            <option value="18">JHARKHAND</option>
                                            <option value="19">KARNATAKA</option>
                                            <option value="3">Kerala</option>
                                            <option value="20">LAKSHADWEEP</option>
                                            <option value="21">MADHYA PRADESH</option>
                                            <option value="2">Maharashtra</option>
                                            <option value="22">MANIPUR</option>
                                            <option value="23">MEGHALAYA</option>
                                            <option value="24">MIZORAM</option>
                                            <option value="25">NAGALAND</option>
                                            <option value="26">NCT OF DELHI</option>
                                            <option value="27">ODISHA</option>
                                            <option value="28">PUDUCHERRY</option>
                                            <option value="29">PUNJAB</option>
                                            <option value="30">RAJASTHAN</option>
                                            <option value="31">SIKKIM</option>
                                            <option value="32">TAMIL NADU</option>
                                            <option selected="selected" value="1">
                                                Telangana
                                            </option>
                                            <option value="33">TRIPURA</option>
                                            <option value="34">UTTAR PRADESH</option>
                                            <option value="35">UTTARAKHAND</option>
                                            <option value="36">WEST BENGAL</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div className="all_forms_div_wrapper">
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        District <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <select
                                            id="cars"
                                            name="carlist"
                                            form="carform"
                                            className="form-control"
                                        >
                                            <option value="">Select District</option>
                                            <option value="1">Adilabad</option>
                                            <option value="9">Bhadradri Kothagudem </option>
                                            <option selected="selected" value="25">
                                                Hyderabad
                                            </option>
                                            <option value="28">Jagithyal</option>
                                            <option value="6">Jangaon</option>
                                            <option value="5">Jayashankar</option>
                                            <option value="17">Jogulamba Gadwal</option>
                                            <option value="2">Kamareddy</option>
                                            <option value="27">Karimnagar</option>
                                            <option value="8">Khammam</option>
                                            <option value="26">Kumuram Bheem (Asifabad)</option>
                                            <option value="7">Mahabubabad</option>
                                            <option value="14">Mahabubnagar</option>
                                            <option value="12">Mancherial</option>
                                            <option value="10">Medak</option>
                                            <option value="22">Medchal-Malkajgiri</option>
                                            <option value="16">Nagarkurnool</option>
                                            <option value="18">Nalgonda</option>
                                            <option value="23">Nirmal</option>
                                            <option value="31">Nizamabad</option>
                                            <option value="29">Peddapalli</option>
                                            <option value="30">Rajanna Sircilla</option>
                                            <option value="24">Ranga Reddy</option>
                                            <option value="11">Sangareddy</option>
                                            <option value="13">Siddipet</option>
                                            <option value="19">Suryapet</option>
                                            <option value="21">Vikarabad</option>
                                            <option value="15">Wanaparthy</option>
                                            <option value="4">Warangal Rural </option>
                                            <option value="3">Warangal Urban</option>
                                            <option value="20">Yadadri Bhuvanagiri</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Mandal <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <select
                                            id="cars"
                                            name="carlist"
                                            form="carform"
                                            className="form-control"
                                        >
                                            <option selected="selected" value="">Select Mandal</option>
                                            <option value="458">
                                                Amberpet
                                            </option>
                                            <option value="472">AMEERPET</option>
                                            <option value="468">ASIF NAGAR</option>
                                            <option value="462">Bahadurpura</option>
                                            <option value="471">BALNAGAR</option>
                                            <option value="457">BANDLAGUDA</option>
                                            <option value="460">Charminar</option>
                                            <option value="469">Golconda</option>
                                            <option value="464">Himayatha Nagar</option>
                                            <option value="467">KHAIRTABAD</option>
                                            <option value="465">Maredpally</option>
                                            <option value="463">Musheerabad</option>
                                            <option value="461">Nampally</option>
                                            <option value="459">Saidabad</option>
                                            <option value="466">Secunderabad</option>
                                            <option value="474">Secunderabad-Cantonment</option>
                                            <option value="473">Secunderabad-Rasoolpura</option>
                                            <option value="470">Shaikpet</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div className="all_forms_div_wrapper">
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Village/City/Town <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <select
                                            id="cars"
                                            name="carlist"
                                            form="carform"
                                            className="form-control"
                                        >
                                            <option value="">Select Village</option>
                                            <option value="20442">ADIKMET</option>

                                            <option value="20443">ADIKMET (Blighted)</option>
                                            <option value="20463">AHMED NAGAR</option>
                                            <option value="20450">AKASH NAGAR</option>
                                            <option value="20257">
                                                Ambedkar Nagar(Slum+Blighted)
                                            </option>
                                            <option value="20432">ANJAIAH NAGAR</option>
                                            <option value="20524">
                                                ANJAIAH NAGAR (Tilak Nagar)
                                            </option>
                                            <option value="20499">ANJUMAN BADA</option>
                                            <option value="20489">ANNAPURNA NAGAR</option>
                                            <option value="20438">AZAMABAD</option>
                                            <option value="20379">AZAMPURA BLIGHTED</option>
                                            <option value="20453">AZATH NAGAR</option>
                                            <option value="20456">BAPU NAGAR</option>
                                            <option value="20535">BATHUKAMMAKUNTA</option>
                                            <option value="20528">BHAGYA NAGAR</option>
                                            <option value="20538">
                                                BHARATH NAGAR (Tilak Nagar )
                                            </option>
                                            <option value="20464">CE COLONY</option>
                                            <option value="20383">CHANCHAL GUDA BLIGHTED</option>
                                            <option value="20457">CHENNA REDDY NAGAR</option>
                                            <option value="20459">CPL QUATERS</option>
                                            <option value="20513">CPL SINGLE QUARTERS </option>
                                            <option value="20434">DAYANAND NAGAR</option>
                                            <option value="20378">DC PATEL COLONY BLIGHTED</option>
                                            <option value="20448">DD COLONY-I-blighted</option>
                                            <option value="20454">DD COLONY-II</option>
                                            <option value="20532">F G NAGAR</option>
                                            <option value="20374">FARATH NAGAR </option>
                                            <option value="20380">FARTH NAGAR BLIGHTED</option>
                                            <option value="20386">GACHI BOWLI</option>
                                            <option value="20428">GANDHI NAGAR</option>
                                            <option value="20486">GANGA NAGAR (Harajpenta)</option>
                                            <option value="20502">HARRAJPENTA</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Pin code <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required

                                            defaultValue={534003}
                                            className="form_input_control"
                                            type="text"
                                        />
                                    </div>
                                </div>

                            </div>
                            <div class="headline">Details of separate bank account as per section 4 (2)(l)(D) of the Act
                            </div>
                            <div className="all_forms_div_wrapper">
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Bank Name <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required

                                            defaultValue={""}
                                            className="form_input_control"
                                            type="text"
                                        />
                                    </div>
                                </div>
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Branch Name<span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required

                                            defaultValue={""}
                                            className="form_input_control"
                                            type="text"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="all_forms_div_wrapper">
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">

                                        Bank A/c Number

                                        <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required

                                            defaultValue={""}
                                            className="form_input_control"
                                            type="text"
                                        />
                                    </div>
                                </div>
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Bank Address<span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required

                                            defaultValue={""}
                                            className="form_input_control"
                                            type="text"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="all_forms_div_wrapper">
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        IFSC Code <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required

                                            defaultValue={""}
                                            className="form_input_control"
                                            type="text"
                                        />
                                    </div>
                                </div>

                            </div>
                            <div class="headline">GIS Details

                            </div>

                            <div className="all_forms_div_wrapper">
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">

                                        Latitude

                                        <span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required

                                            defaultValue={""}
                                            className="form_input_control"
                                            type="text"
                                        />
                                    </div>
                                </div>
                                <div className="details_fill_wrapper">
                                    <p className="ot_text">
                                        Longitude<span style={{ color: "red" }}>*</span>
                                    </p>
                                    <div className="input_div_wrapper">
                                        <input
                                        required

                                            defaultValue={""}
                                            className="form_input_control"
                                            type="text"
                                        />
                                    </div>
                                </div>
                            </div>

                        </div>
                        <center>
                        <input
                        required

                   onClick={()=>{handelpost()}}
                  defaultValue={""}
                    
                    type="submit"
                    name="Save profile"
                    style={{
                      width: "120px",
                      height: "40px",
                      backgroundColor: "#5bc0de",
                      borderColor: "1px solid #5bc0de",
                      borderRadius: "2px",
                      color: "white",
                      fontSize: "14px",
                    }}
                  />
                        </center>
                        
                        </form>
                        <table className="tbl_memberData" >
                            <tbody>
                                <tr className="heading_row">
                                    <th > Sr No.</th>
                                    <th>Project Name</th>
                                    <th>Boundaries East</th>
                                    <th>Boundaries West</th>
                                    <th>Boundaries North</th>
                                    <th>Boundaries South</th>
                                    <th > Total Area(In sqmts)</th>
                                    <th>Total Building Units</th>
                                    <th>Action</th>
                                </tr>
                                {formdata?.map((member) => (
                                    <tr key={member.id} className="row_value_text">
                                           <td >{member.id}</td>
                                        <td >{member.projectname}</td>
                                        <td>{member.East}</td>
                                        <td>{member.west}</td>
                                        <td>{member.north}</td>
                                        <td>{member.south}</td>
                                        <td>{member.area}</td>
                                        <td className="DesiValue">{member.area}</td>
                                        
                                       
                                        <td>
                                            <img
                                                src="../Images/Edit.jpg"
                                                onClick={() => handleEdit(member.id)}
                                                width="20px"
                                                height="20px"
                                                alt="Edit"
                                            />
                                            {' | '}
                                            <img
                                                src="../Images/delete.png"
                                                onClick={() => handleDelete(member.id)}
                                                width="20px"
                                                height="20px"
                                                alt="Delete"
                                            />
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table></>

                </Typography>
                <Typography paragraph sx={{ p: 2, backgroundColor: "#f6f7fb" }}>


                </Typography>
            </Box>
        </Box>
    );
}
